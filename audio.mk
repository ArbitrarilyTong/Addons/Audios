LOCAL_PATH := ArbitrarilyTong/Audios/sounds

# Copy audio files to output directory
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/notifications/ogg/Argon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Argon.ogg \
    $(LOCAL_PATH)/notifications/ogg/ATOMS.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/ATOMS.ogg \
    $(LOCAL_PATH)/notifications/ogg/CRYSTALS.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/CRYSTALS.ogg \
    $(LOCAL_PATH)/notifications/ogg/FLUX.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/FLUX.ogg \
    $(LOCAL_PATH)/notifications/ogg/GLITTER.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/GLITTER.ogg \
    $(LOCAL_PATH)/ringtones/ogg/ArbitrarilyTongTune.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/ArbitrarilyTongTune.ogg

# Default notification/alarm sounds
# PRODUCT_PRODUCT_PROPERTIES -= ro.config.notification_sound=Argon.ogg
# PRODUCT_PRODUCT_PROPERTIES += ro.config.notification_sound=FLUX.ogg

# Default ringtone
# PRODUCT_PRODUCT_PROPERTIES -= ro.config.notification_sound=Orion.ogg
# PRODUCT_PRODUCT_PROPERTIES += ro.config.ringtone=ArbitrarilyTongTune.ogg

# Override the default notification/alarm sounds and ringtone
# See more: https://android.googlesource.com/platform/build/+/master/Changes.md
# PRODUCT_VENDOR_PROPERTIES += \
#     ro.config.notification_sound=FLUX.ogg \
#     ro.config.ringtone=ArbitrarilyTongTune.ogg

# You can only change your default audio files by editing the following files:
# - vendor/lineage/config/common_mobile.mk
# link: https://github.com/LineageOS/android_vendor_lineage/commit/eb321ebe266e4197990d2a89ed9400460e7e0df1
# See more: https://android.googlesource.com/platform/build/+/master/Changes.md